package com.conygre.spring.entities;

import org.bson.types.ObjectId;
import org.springframework.data.mongodb.core.mapping.Document;

@Document
public class CompactDisc {

    public CompactDisc(String title, String artist, double price) {
        this.title = title;
        this.artist = artist;
        this.price = price;
    }

    public CompactDisc() {
    }

    public void setId(ObjectId id) {
        this.id = id;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setArtist(String artist) {
        this.artist = artist;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    ObjectId id;
    String title;
    String artist;
    double price;

    public ObjectId getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public String getArtist() {
        return artist;
    }

    public double getPrice() {
        return price;
    }
}
