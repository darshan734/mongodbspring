package com.conygre.spring;

import com.conygre.spring.entities.CompactDisc;
import com.conygre.training.MongoJavaConfig;
import com.mongodb.BasicDBObject;
import org.junit.After;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.AnnotationConfigContextLoader;

import java.util.List;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = MongoJavaConfig.class, loader = AnnotationConfigContextLoader.class)

public class BasicMongoTest {

    @Autowired
    MongoTemplate mongoTemplate;

    @Test
    public void canInsertSuccessfully() {
        CompactDisc disc1 = new CompactDisc("Gold","Carrot", 12.99);
        CompactDisc disc2 = new CompactDisc("Silver","John", 14.99);
        CompactDisc disc3 = new CompactDisc("Bronze","Bill", 13.99);
        mongoTemplate.insert(disc1);
        mongoTemplate.insert(disc2);
        mongoTemplate.insert(disc3);

        List<CompactDisc> discs = mongoTemplate.findAll(CompactDisc.class);
        discs.forEach(disc -> System.out.println(disc.getTitle()));
        Assert.assertEquals(3, discs.size());
    }

    // need to comment this method to save above recs to database
    @After
    public void cleanUp() {
        for(String collectionName : mongoTemplate.getCollectionNames()) {
            if(!collectionName.startsWith("system.")) {
                mongoTemplate.getCollection(collectionName).remove(new BasicDBObject());
            }
        }
    }
}
